package DataView;

import java.awt.BorderLayout;
import java.awt.Color;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;


/**
 * Class to display the simulation results
 * @author Felipe Lisboa
 */
public class GraphResults extends ApplicationFrame{
    
    String chartTitle;
    
    XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
    XYSeries series = new XYSeries("Baseline");
    XYSeries series2 = new XYSeries("Degraded");
    XYSeries series3 = new XYSeries("Degraded and Rescheduled");
    
    public XYPlot plot;
    
    public GraphResults(String applicationTitle, String chartTitle) {
        
        super(applicationTitle);
        
        this.chartTitle = chartTitle;
        
        xySeriesCollection.addSeries(series);
        final ChartPanel chartPanel = createDemoPanel();
        chartPanel.setBackground( Color.WHITE );
        
        this.add(chartPanel, BorderLayout.CENTER);
    }

    private ChartPanel createDemoPanel() {
        JFreeChart jfreechart = ChartFactory.createXYLineChart(
            chartTitle, "load", "%", createSampleData(),
            PlotOrientation.VERTICAL, true, true, false);
        
        jfreechart.getPlot().setBackgroundPaint( Color.lightGray );
        
        XYPlot xyPlot = (XYPlot) jfreechart.getPlot();
        xyPlot.setDomainCrosshairVisible(true);
        xyPlot.setRangeCrosshairVisible(true);
        XYItemRenderer renderer = xyPlot.getRenderer();
        renderer.setSeriesPaint(0, Color.blue);
        renderer.setSeriesPaint(2, Color.ORANGE);
        NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
        domain.setVerticalTickLabels(true);
        return new ChartPanel(jfreechart);
    }

    private XYDataset createSampleData() {
        XYSeriesCollection xySeriesCollection = new XYSeriesCollection();
        series = new XYSeries("Baseline");
        series2 = new XYSeries("Degraded");
        series3 = new XYSeries("Degraded and Rescheduled");
       
        xySeriesCollection.addSeries(series);
        xySeriesCollection.addSeries(series2);
        xySeriesCollection.addSeries(series3);
        return xySeriesCollection;
    }
    
    /**
     * Add value to the graph
     * @param dado 
     */
    public void addDado(int load, float dado, int chartSelected){
        switch(chartSelected){
            case 0:
                series.add(load, dado);
                break;
            case 1:
                series2.add(load, dado);
                break;
            case 2:
                series3.add(load, dado);
                break;
        }
    }
    
    /**
     * Draw serie from dataset
     * @param dataset
     * @param chartSelected 
     */
    public void drawSerie(float dataset[][], int chartSelected){
        
    }
    
    /**
     * 
     * @param dataset
     * @param steps 
     */
    public void drawChart(float dataset[], int steps){
        
    }
}