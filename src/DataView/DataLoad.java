package DataView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Load resources if exist, save the current analisys and iterate continued
 * process it uses CSV file
 *
 * @author Felipe Lisboa
 */
public class DataLoad {

    String csvFile;

    /**
     * 
     * @param csvFile the name of results file
     * @return 
     */
    public String readData(String csvFile) {
        this.csvFile = csvFile;

        BufferedReader crunchifyBuffer = null;
        String crunchifyLine;
        try {
            crunchifyBuffer = new BufferedReader(new FileReader("teste.csv"));
            
            try {
                // How to read file in java line by line?
                /*while ((crunchifyLine = crunchifyBuffer.readLine()) != null) {
                    System.out.println(crunchifyLine);
                }*/
                
                crunchifyLine = crunchifyBuffer.readLine();
                
                return crunchifyLine;
                
            } catch (IOException ex) {
                Logger.getLogger(DataLoad.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            System.out.println(crunchifyBuffer);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataLoad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    /**
     * 
     * @param filename the name of results file
     * @return 
     */
    public boolean saveData(String filename,float[][] dataset, int step) {
        FileWriter pw = null; 
        try {
            pw = new FileWriter(filename,true);
        } catch (IOException ex) {
            Logger.getLogger(DataLoad.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        StringBuilder sb = new StringBuilder();
        sb.append("id");
        sb.append(';');
        sb.append("Name");
        sb.append('\n');

        sb.append("1");
        sb.append(';');
        sb.append("Prashant Ghimire");
        sb.append('\n');
        
        sb.append(';');
        sb.append(';');
        sb.append("TESTE");

        try {
            pw.write(sb.toString());
            pw.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(DataLoad.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}