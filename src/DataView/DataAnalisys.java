package DataView;

import java.util.Arrays;

/**
 * @author Felipe Lisboa
 */
public class DataAnalisys {
    
    public float dataset[][];
    
    public DataAnalisys(){
        
    }
    
    
    /**
     * 
     * @param data
     * @param pos
     * @param iteration 
     */
    public void addData(float data, int pos, int iteration){
        dataset[pos][iteration] = data;
    }
    
    /**
     * 
     * @param pos
     * @param iteration
     * @return 
     */
    public float getData(int pos, int iteration){
        return dataset[pos][iteration];
    }
    
    /**
     * 
     * @return 
     */
    public float[][] getDataSet(){
        return dataset;
    }
    
    /**
     * 
     * @param dataset 
     */
    public void setDataSet(float[][] dataset){
        this.dataset = dataset;
    }
        
    /**
     * 
     * @param iterations
     * @param steps 
     */
    public void createDataset(int iterations, int steps){
        dataset = new float[iterations][steps];
    }
    
    /**
     * receive two values or datasets and return their average
     * @param dataset
     * @return 
     */
    public float[] average(float dataset[][]){
        float result[] = new float[dataset[1].length];
        
        for(int i = 0; i < dataset[1].length; i++){
            result[i] = 0;
            for (float[] dataset1 : dataset) {
                result[i] += dataset1[i];
            }
            result[i] /= dataset.length;
        }
        
        return result;
    }
    
    /**
     * 
     * @param dataset
     * @return 
     */
    public float standardDeviation(float dataset[][]){
        
        float averageDataset[] = new float[dataset[1].length];
        float average = 0;
        float result = 0;
        
        for(int i = 0; i < dataset[1].length; i++){
            averageDataset[i] = 0;
            for (float[] dataset1 : dataset) {
                averageDataset[i] += dataset1[i];
            }
            averageDataset[i] /= dataset.length;
        }
        
        for(int i = 0; i < averageDataset.length; i++){
            average += averageDataset[i];
        }
        average /= averageDataset.length;
        
        for(int i = 0; i <  averageDataset.length; i++){
            result += Math.pow((average - averageDataset[i]),2);
        }
        
        return (float)Math.sqrt(result);
    }
    
    public void printDataset(){
        System.out.println("Imprimindo o array");
        /*for(int i = 0; i < dataset.length; i ++){
            System.out.println(dataset[0][i]);
        }*/
        
        /*for(int i = 0; i < dataset[1].length; i++){
            
            for(int j = 0; j < dataset.length; j++)
            
        }*/
        
        for (float[] arr : dataset) {
            System.out.println(Arrays.toString(arr));
        }
    }
    
    /**
     * 
     * @param file
     * @param datasets 
     */
    public void saveData(String file, float datasets[][]){
        
    }
}