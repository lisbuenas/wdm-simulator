package wdmsim.rwa;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import wdmsim.util.WeightedGraph;
import wdmsim.util.Dijkstra;
import wdmsim.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.TreeSet;

/**
 *
 * @author andred
 */
public class FixedAlternateShortestPathFirstFitRWA implements RWA {

    private PhysicalTopology pt;
    private VirtualTopology vt;
    private ControlPlaneForRWA cp;
    private WeightedGraph graph;
    private int pathSelectionMode = 5;
    private int alternativePaths = 5;
    private int wavelengthConversionMode = 2;
    private double arrivalTime;
    public int linkFailed = 0;

    @Override
    public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp) {
        this.pt = pt;
        this.vt = vt;
        this.cp = cp;
        this.graph = pt.getWeightedGraph();
    }

    /* pathSelectionMode:
     * 1- edge disjoint;
     * 2- remove first edge only;
     * 3- remove last edge only;
     * 4- remove a random edge;
     * 5- set a infinity cost to all the edges.
     * 
     * wavelengthConversionMode:
     * 1- First-Fit
     * 2- Converter-Aware-First-Fit
     * 3- FLR
     */
    public void setParameters(int pathSelectionMode, int alternativePaths, int wavelengthConversionMode) {
        this.pathSelectionMode = pathSelectionMode;
        this.alternativePaths = alternativePaths;
        this.wavelengthConversionMode = wavelengthConversionMode;
    }

    public void flowArrival(Flow flowInfo) {
        int aux;
        long id;
        int numWvls;

        ArrayList<Integer> activeLinks = new ArrayList<Integer>();
        ArrayList<Integer> activeWvls = new ArrayList<Integer>();
        int[] linksArray = {};
        int[] wvlsArray = {};

        int[] nodes, links, wvls;
        LightPath[] lps = new LightPath[1];
        WeightedGraph g;
        this.arrivalTime = flowInfo.getArrivalTime();

        // Try existent LPs first (GROOMING)
        lps[0] = getGroomingLightpath(flowInfo);
        if (lps[0] instanceof LightPath) {
            if (cp.acceptFlow(flowInfo.getID(), lps)) {//not pass with the current topology
                return;
            }
        }

        //New path
        g = new WeightedGraph(this.graph);

        for (int x = 0; x < alternativePaths; x++) {
            // Shortest-Path
            nodes = Dijkstra.getShortestPath(g, flowInfo.getSource(), flowInfo.getDestination());

            if (nodes.length == 0) {
                // No possible path found
                cp.blockFlow(flowInfo.getID());
                return;
            }

            links = getLinks(nodes);

            for (int i = 0; i < links.length; i++) {//find error link
                if (links[i] != 10) {
                    activeLinks.add(links[i]);
                }
            }

            // First-Fit
            //wvls = new int[links.length];
            wvls = new int[activeLinks.size()];

            numWvls = pt.getLink(links[0]).getWavelengths();

            linksArray = new int[activeLinks.size()];
            wvlsArray = new int[activeWvls.size()];

            for (int j = 0; j < activeLinks.size(); j++) {
                linksArray[j] = activeLinks.get(j).intValue();
                wvlsArray[j] = activeWvls.get(j).intValue();
            }

            for (int i = 0; i < numWvls; i++) {
                for (int j = 0; j < wvls.length; j++) {
                    wvls[j] = i;
                }
                //passando zero como HT, pq inicialmente nao ha caminhos estabelecidos
                if ((id = vt.createLightpath(linksArray, wvlsArray, 0, this.arrivalTime)) >= 0) {
                    lps[0] = vt.getLightpath(id);
                    cp.acceptFlow(flowInfo.getID(), lps);
                    return;
                }
            }

            // Wavelength Conversion
            if (links.length > 1) {
                wvls = wavelengthConversion(linksArray, wvlsArray);
                if (wvls != null) {
                    if ((id = vt.createLightpath(linksArray, wvlsArray, 0, this.arrivalTime)) >= 0) {
                        lps[0] = vt.getLightpath(id);
                        cp.acceptFlow(flowInfo.getID(), lps);
                        return;
                    }
                }
            }

            switch (pathSelectionMode) {
                case 1:
                    // Remove all the shortest-path links
                    for (int i = 0; i < nodes.length - 1; i++) {
                        g.removeEdge(nodes[i], nodes[i + 1]);
                    }
                    break;
                case 2:
                    // Remove the first shortest-path link
                    g.removeEdge(nodes[0], nodes[1]);
                    break;
                case 3:
                    // Remove the last shortest-path link
                    g.removeEdge(nodes[nodes.length - 2], nodes[nodes.length - 1]);
                    break;
                case 4:
                    // Remove a random link
                    Random r = new Random();
                    int e = r.nextInt(nodes.length - 1);
                    g.removeEdge(nodes[e], nodes[e + 1]);
                    break;
                case 5:
                    // Set all the shortest-path links with infinity weigth
                    for (int i = 0; i < nodes.length - 1; i++) {
                        g.setWeight(nodes[i], nodes[i + 1], 9999999); // Integer.MAX_VALUE;
                    }
                    break;
            }
        }

        // Block flow
        cp.blockFlow(flowInfo.getID());
    }

    @Override
    public void flowDeparture(long id) {
    }

//    @Override
    //public void batchArrival(Batch batch) {
    //}
    public LightPath getPossibleNewLightpath(int src, int dst, int bw) {
        int numWvls;
        int[] nodes, links, wvls;
        WeightedGraph g;

        ArrayList<Integer> activeLinks = new ArrayList<Integer>();
        ArrayList<Integer> activeWvls = new ArrayList<Integer>();
        int linksArray[] = {};//resultant
        int wvlsArray[] = {};//resultant

        g = new WeightedGraph(this.graph);
        for (int x = 0; x < alternativePaths; x++) {

            // Shortest-Path
            nodes = Dijkstra.getShortestPath(g, src, dst);
            if (nodes.length < 2) {
                // No possible path found
                return null;
            }
            
            
            for (int i = 0; i < nodes.length; i++) {
                if (nodes[i] == linkFailed) {
                    g.removeEdge(pt.getLink(linkFailed).src, pt.getLink(linkFailed).dst);

                }
            }

            nodes = Dijkstra.getShortestPath(g, src, dst);


            links = getLinks(nodes);

            // First-Fit
            wvls = new int[links.length];
            numWvls = pt.getNumWavelengths();

            for (int i = 0; i < numWvls; i++) {
                for (int j = 0; j < wvls.length; j++) {
                    wvls[j] = i;
                }
                if (vt.canCreateLightpath(links, wvls)) {
                    //zero time life

                    /*if (linksArray.length > 0 && wvlsArray.length > 0) {
                        if (vt.canCreateLightpath(linksArray, wvlsArray)) {
                            return new LightPath(0, src, dst, linksArray, wvlsArray);
                        } else {
                            return null;
                        }
                    }*/
                    
                    if (links.length > 0 && wvls.length > 0) {
                        if (vt.canCreateLightpath(links, wvls)) {
                            return new LightPath(0, src, dst, links, wvls);
                        } else {
                            return null;
                        }
                    }

                    //return new LightPath(0, src, dst, links, wvls);
                }
            }

            // Wavelength Conversion
            /*if (linksArray.length > 1) {
                wvls = wavelengthConversion(linksArray, wvls);
                if (wvls != null) {
                    if (vt.canCreateLightpath(linksArray, wvls)) {
                        //zero time life
                        return new LightPath(0, src, dst, linksArray, wvls);
                    }
                }
            }*/
            // Wavelength Conversion
            if (links.length > 1) {
                wvls = wavelengthConversion(links, wvls);
                if (wvls != null) {
                    if (vt.canCreateLightpath(links, wvls)) {
                        //zero time life
                        return new LightPath(0, src, dst, links, wvls);
                    }
                }
            }
            switch (pathSelectionMode) {
                case 1:
                    // Remove all the shortest-path links
                    for (int i = 0; i < nodes.length - 1; i++) {
                        g.removeEdge(nodes[i], nodes[i + 1]);
                    }
                    break;
                case 2:
                    // Remove the first shortest-path link
                    g.removeEdge(nodes[0], nodes[1]);
                    break;
                case 3:
                    // Remove the last shortest-path link
                    g.removeEdge(nodes[nodes.length - 2], nodes[nodes.length - 1]);
                    break;
                case 4:
                    // Remove a random link
                    Random r = new Random();
                    int e = r.nextInt(nodes.length - 1);
                    g.removeEdge(nodes[e], nodes[e + 1]);
                    break;
                case 5:
                    // Set all the shortest-path links with high weigth
                    for (int i = 0; i < nodes.length - 1; i++) {
                        g.setWeight(nodes[i], nodes[i + 1], 9999999); // Integer.MAX_VALUE;
                    }
                    break;
            }
        }
        return null;
    }

    public LightPath getPossibleNewLightpathLimitHops(int src, int dst, int bw, int hops) {
        int numWvls;
        int[] nodes, links, wvls;
        WeightedGraph g;

        ArrayList<Integer> activeLinks = new ArrayList<Integer>();
        ArrayList<Integer> activeWvls = new ArrayList<Integer>();
        int[] linksArray = {};
        int[] wvlsArray = {};

        g = new WeightedGraph(this.graph);

        for (int x = 0; x < alternativePaths; x++) {
            // Shortest-Path
            nodes = Dijkstra.getShortestPath(g, src, dst);

            if (nodes.length < 2) {
                // No possible path found
                return null;
            }
            if (nodes.length - 1 >= 1 && nodes.length - 1 <= hops) {

                links = getLinks(nodes);

                // First-Fit
                wvls = new int[links.length];
                numWvls = pt.getNumWavelengths();
                for (int i = 0; i < numWvls; i++) {
                    for (int j = 0; j < wvls.length; j++) {
                        wvls[j] = i;
                    }
                    if (vt.canCreateLightpath(links, wvls)) {
                        //zero time life
                        return new LightPath(0, src, dst, links, wvls);
                    }
                }

                // Wavelength Conversion
                if (links.length > 1) {
                    wvls = wavelengthConversion(links, wvls);
                    if (wvls != null) {
                        if (vt.canCreateLightpath(links, wvls)) {
                            //zero time life
                            return new LightPath(0, src, dst, links, wvls);
                        }
                    }
                }

                switch (pathSelectionMode) {
                    case 1:
                        // Remove all the shortest-path links
                        for (int i = 0; i < nodes.length - 1; i++) {
                            g.removeEdge(nodes[i], nodes[i + 1]);
                        }
                        break;
                    case 2:
                        // Remove the first shortest-path link
                        g.removeEdge(nodes[0], nodes[1]);
                        break;
                    case 3:
                        // Remove the last shortest-path link
                        g.removeEdge(nodes[nodes.length - 2], nodes[nodes.length - 1]);
                        break;
                    case 4:
                        // Remove a random link
                        Random r = new Random();
                        int e = r.nextInt(nodes.length - 1);
                        g.removeEdge(nodes[e], nodes[e + 1]);
                        break;
                    case 5:
                        // Set all the shortest-path links with high weigth
                        for (int i = 0; i < nodes.length - 1; i++) {
                            g.setWeight(nodes[i], nodes[i + 1], 9999999); // Integer.MAX_VALUE;
                        }
                        break;
                }
            }
        }
        return null;
    }

    private LightPath getGroomingLightpath(Flow flowInfo) {
        int aux, num = 0;
        LightPath lpant, lp = null;
        if (vt.hasLightpath(flowInfo.getSource(), flowInfo.getDestination())) {
            //System.out.println("Sim!");
            TreeSet<LightPath> lps = vt.getLightpaths(flowInfo.getSource(), flowInfo.getDestination());
            if (lps != null && !lps.isEmpty()) {
                //if (lps.size() > 1) {
                //System.out.println("Possiveis: "+Integer.toString(lps.size()));
                //}
                while (!lps.isEmpty()) {
                    lpant = lps.pollFirst();

                    if (vt.getLightpathBWAvailable(lpant.getID()) >= flowInfo.getRate()) {
                        aux = cp.getLightpathFlowCount(lpant.getID());
                        if (aux > num) {
                            num = aux;
                            lp = lpant;
                            //break;
                        }
                    }
                }
                return lp;
            }
        }
        return null;
    }

    private int[] getLinks(int[] nodes) {

        int[] links;
        links = new int[nodes.length - 1];
        for (int j = 0; j < nodes.length - 1; j++) {
            links[j] = pt.getLink(nodes[j], nodes[j + 1]).getID();
        }

        return links;
    }

    private int[] wavelengthConversion(int[] links, int[] wvls) {

        int wvl, lastwvl, conv;
        switch (wavelengthConversionMode) {
            case 1:
                // First-Fit
                for (int i = 0; i < wvls.length; i++) {
                    wvl = pt.getLink(links[i]).firstWLAvailable();
                    if (wvl < 0) {
                        wvl = 0;
                    }
                    wvls[i] = wvl;
                }
                break;
            case 2:
                // Converter-Aware-First-Fit
                wvl = -1;
                lastwvl = -1;
                conv = 0;
                for (int i = 0; i < wvls.length; i++) {
                    // If has lastwvl
                    if (lastwvl != -1) {
                        // Try last wvl first
                        if (pt.getLink(links[i]).isWLAvailable(lastwvl)) {
                            wvl = lastwvl;
                        } else // Find a new viable wvl
                         if (pt.getNode(pt.getLink(links[i]).getSource()).hasFreeWvlConverters()) {
                                conv = pt.getNode(pt.getLink(links[i]).getSource()).getWvlConversionRange();
                                for (int w = lastwvl - conv; w <= lastwvl + conv; w++) {
                                    if (w >= 0 && w < pt.getNumWavelengths()) {
                                        if (pt.getLink(links[i]).isWLAvailable(w)) {
                                            lastwvl = wvl = w;
                                            break;
                                        }
                                    }
                                }
                            }
                    } else {
                        // Get first available wvl
                        lastwvl = wvl = pt.getLink(links[i]).firstWLAvailable();
                    }
                    // Check the wvl
                    if (wvl > -1) {
                        wvls[i] = wvl;
                    } else {
                        break;
                    }
                }
                break;
            case 3:
                // First-Longest-Lambda-Run
                wvls = FLR(links);
                break;
        }
        return wvls;
    }

    private int[] FLR(int[] links) {
        int i,
                j, p,
                s,
                lmax;
        int[] wvls;
        class run {

            private int wvl;
            private ArrayList<Integer> links;

            public run(int wvl) {
                this.wvl = wvl;
                links = new ArrayList<Integer>();
            }

            public void addLink(int id) {
                links.add(id);
                Collections.sort(links);
            }

            public int getWavelength() {
                return wvl;
            }

            public int getFirstLink() {
                return links.get(0).intValue();
            }

            public int getLastLink() {
                return links.get(links.size() - 1).intValue();
            }

            public int getSize() {
                return links.size();
            }

            @Override
            public String toString() {
                return links.toString();
            }
        }
        run r;
        ArrayList<run> allruns = new ArrayList<run>();
        ArrayList<run> runs = new ArrayList<run>();

        // Define all possible runs
        allruns.clear();
        for (i = 0; i < pt.getNumWavelengths(); i++) {
            j = links.length - 1;
            while (j >= 0) {
                if (pt.getLink(links[j]).isWLAvailable(i)
                        && (j == links.length - 1 || pt.getNode(pt.getLink(links[j]).getSource()).hasFreeWvlConverters())) {
                    r = new run(i);
                    r.addLink(j);
                    j--;
                    while (j >= 0 && pt.getLink(links[j]).isWLAvailable(i)) {
                        r.addLink(j);
                        j--;
                    }
                    allruns.add(r);
                } else {
                    j--;
                }
            }
        }
        if (allruns.size() == 0) {
            return null;
        }

        // Select subset from runs
        runs.clear();
        i = 0;
        p = -1;
        s = -1;
        while (i < links.length) {
            lmax = -1;
            for (int k = 0; k < allruns.size(); k++) {
                if (allruns.get(k).getFirstLink() > i) {
                    continue;
                }
                if (allruns.get(k).getLastLink() > lmax
                        && (p == -1 || Math.abs(p - allruns.get(k).getWavelength()) <= pt.getNode(pt.getLink(links[i]).getSource()).getWvlConversionRange())) {
                    s = k;
                    lmax = allruns.get(k).getLastLink();
                }
            }
            if (lmax == -1) {
                return null;
            }
            p = allruns.get(s).getWavelength();
            runs.add(allruns.get(s));
            allruns.remove(s);

            i = lmax + 1;

        }

        // Based on subset, assign wvls to all links
        i = 0;
        wvls = new int[links.length];
        for (j = 0; j < runs.size(); j++) {
            while (i <= runs.get(j).getLastLink()) {
                wvls[i] = runs.get(j).getWavelength();
                i++;
            }
        }
        // Returns
        return wvls;
    }

    /*@Override
    public void flowArrival(Flow flow) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }*/
    @Override
    public void setLinkError(int link) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSimulationOption(int option) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getSimulationOption() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setEventScheduler(EventScheduler es) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
