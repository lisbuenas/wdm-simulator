/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package wdmsim.rwa;

import wdmsim.*;

/**
 * This is the interface that provides some methods for the RWA class.
 * These methods basically deal with the simulation interface and with
 * the arriving and departing flows.
 * 
 * The Routing and Wavelength Assignment (RWA) is a optical networking problem
 * that has the goal of maximizing the number of optical connections.
 * 
 * @author andred
 */
public interface RWA {
    
    public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp);

    public void flowArrival(Flow flow);
    
    public void flowDeparture(long id);
    
    public void setLinkError(int link);
    
    public void setSimulationOption(int option);
    
    public int getSimulationOption();
    
    public void setEventScheduler(EventScheduler es);
}
