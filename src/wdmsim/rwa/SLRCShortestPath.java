/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim.rwa;

import java.util.Date;
import wdmsim.util.WeightedGraph;
import wdmsim.util.Dijkstra;
import wdmsim.*;
import java.util.TreeSet;

/**
 *
 * @author andred
 */
public class SLRCShortestPath implements RWA {

    private PhysicalTopology pt;
    private VirtualTopology vt;
    private ControlPlaneForRWA cp;
    private WeightedGraph graph;
    private FixedAlternateShortestPathFirstFitRWA routingModule;
    private Tracer tr = Tracer.getTracerObject();
    private double arrivalTime;

    @Override
    public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp) {
        this.pt = pt;
        this.vt = vt;
        this.cp = cp;
        this.graph = pt.getWeightedGraph();

        routingModule = new FixedAlternateShortestPathFirstFitRWA();
        routingModule.simulationInterface(pt, vt, cp);
        routingModule.setParameters(5, 5, 2);
    }

    //public void batchArrival(Batch batch) {
    //}
    @Override
    public void flowArrival(Flow flow) {
        long time = new Date().getTime();
        long flowID;
        int src, dst, bw;
        int[] nodesaux;
        WeightedGraph g;
        LightPath lp;
        LightPath[] lps;
        boolean accepted;
        double holdingTime;
        double delayTolerance;
        double arrivalTime;
        double epsilon = 0.00001;

        double limiTime;
        double nextDeparture = 0;
        double newDelayTolerance = 0;
        double newArrivalTime = 0;
        double newDeparture = 0;
        this.arrivalTime = flow.getArrivalTime();

        flowID = flow.getID();
        src = flow.getSource();
        dst = flow.getDestination();
        bw = flow.getRate();
        holdingTime = flow.getDuration();
        arrivalTime = flow.getArrivalTime();
        delayTolerance = flow.getDelayTolerance();

        g = new WeightedGraph(pt.getNumNodes());
        

        for (int i = 0; i < g.size(); i++) {
            for (int j = 0; j < g.size(); j++) {
                if (i != j && i != dst && j != src) {
                    lp = getCheaperLightpath(i, j, bw);
                    if (lp != null) {
                        g.addEdge(i, j, getLightpathCost(lp));
                    }
                }
            }
        }

        // 3- Calcula o menor caminho no grafo auxiliar;
        nodesaux = Dijkstra.getShortestPath(g, src, dst);
        if (nodesaux.length > 1) {
            // 4- Implementa o novo caminho na rede;
            lps = new LightPath[nodesaux.length - 1];
            accepted = true;
            for (int i = 0; i < nodesaux.length - 1; i++) {
                lp = implementCheaperLightpath(nodesaux[i], nodesaux[i + 1], bw, holdingTime);
                if (lp != null) {
                    lps[i] = lp;
                } else {
                    accepted = false;
                    break;
                }
            }
            if (accepted) {
                tr.addRounds(1);
                //tr.addLPCost(getPathCost(lps));
                //tr.addExecTime(new Date().getTime() - time);
                cp.acceptFlow(flowID, lps);
                return;
            }
        }

        //it's not possible to provision the current demand, so if it is delay tolerante can be possible
        //reScheduled it to try meet in next moment
        if (delayTolerance > 0.000000000000000001 && flow.getRounds() > 0) {

            limiTime = (arrivalTime + delayTolerance + (epsilon * delayTolerance));
            nextDeparture = cp.nextDeparture();
            if (nextDeparture < limiTime) {
                newDelayTolerance = 0;
                newArrivalTime = arrivalTime + delayTolerance;
                // newDelayTolerance = calcToleranceDT(arrivalTime, delayTolerance, nextDeparture);
                //newArrivalTime = calcArrivalDT(delayTolerance, nextDeparture, epsilon);

                newDeparture = newArrivalTime + holdingTime;
                cp.reScheduleEvent(flowID, newArrivalTime, newDeparture, newDelayTolerance);
                return;
            }
        }

        //tr.addExecTime(new Date().getTime() - time);
        cp.blockFlow(flowID);
        return;
    }

    @Override
    public void flowDeparture(long id) {
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @param holdingTime
     * @return
     */
    private LightPath implementCheaperLightpath(int src, int dst, int bw, double holdingTime) {
        long id;
        LightPath newLp, groomLp, lp;

        groomLp = getCheaperGroomingLightpath(src, dst, bw);
        newLp = getPossibleNewLightpath(src, dst, bw);
        if (groomLp != null || newLp != null) {
            if (groomLp != null && newLp != null) {
                if (compareLightpaths(groomLp, newLp) == 1) {
                    lp = newLp;
                    id = vt.createLightpath(lp.getLinks(), lp.getWavelengths(), holdingTime, this.arrivalTime);
                    lp = vt.getLightpath(id);
                } else {
                    lp = groomLp;
                }

            } else if (groomLp != null) {
                lp = groomLp;
            } else {
                lp = newLp;
                id = vt.createLightpath(lp.getLinks(), lp.getWavelengths(), holdingTime, this.arrivalTime);
                lp = vt.getLightpath(id);
            }
            return lp;
        }
        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath getCheaperLightpath(int src, int dst, int bw) {
        LightPath newLp, groomLp, lp;

        groomLp = getCheaperGroomingLightpath(src, dst, bw);
        newLp = getPossibleNewLightpath(src, dst, bw);
        if (groomLp != null || newLp != null) {
            if (groomLp != null && newLp != null) {
                if (compareLightpaths(groomLp, newLp) == 1) {
                    lp = newLp;
                } else {
                    lp = groomLp;
                }

            } else if (groomLp != null) {
                lp = groomLp;
            } else {
                lp = newLp;
            }
            return lp;
        }
        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath getCheaperGroomingLightpath(int src, int dst, int bw) {
        LightPath lbest = null;

        if (vt.hasLightpath(src, dst)) {
            TreeSet<LightPath> lps = vt.getLightpaths(src, dst);
            for (LightPath lp : lps) {
                if (vt.getLightpathBWAvailable(lp.getID()) >= bw) {
                    if (lbest == null) {
                        lbest = lp;
                    } else if (compareLightpaths(lbest, lp) == 1) {
                        lbest = lp;
                    }
                }
            }
            return lbest;
        }

        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath getPossibleNewLightpath(int src, int dst, int bw) {
        LightPath lp = null;

        lp = routingModule.getPossibleNewLightpath(src, dst, bw);

        return lp;
    }

    /**
     *
     * @param lp
     * @return
     */
    private double getLightpathCost(LightPath lp) {

        double numHops = (double) vt.hopCount(lp);

        return numHops;
    }

    /**
     * 
     * @param lp1
     * @param lp2
     * @return 
     */
    private int compareLightpaths(LightPath lp1, LightPath lp2) {

        double totalCost1, totalCost2;

        totalCost1 = getLightpathCost(lp1);
        totalCost2 = getLightpathCost(lp2);

        if (totalCost1 < totalCost1) {
            return -1;
        }

        if (totalCost1 > totalCost1) {
            return 1;
        }

        return 0;
    }

    /**
     * 
     * @param lps
     * @return 
     */
    private double getPathCost(LightPath[] lps) {
        double totalCost = 0;

        for (int i = 0; i
                < lps.length; i++) {
            totalCost += getLightpathCost(lps[i]);
        }

        return totalCost;
    }

    private int comparePaths(LightPath[] lps1, LightPath[] lps2) {

        double totalCost1, totalCost2;

        totalCost1 = getPathCost(lps1);
        totalCost2 = getPathCost(lps2);

        if (totalCost1 < totalCost1) {
            return -1;
        }

        if (totalCost1 > totalCost1) {
            return 1;
        }

        return 0;
    }

    /**
     * 
     * @param arrivalTime
     * @param delayTolerance
     * @param nextDeparture
     * @return 
     */
    public double calcToleranceDT(double arrivalTime, double delayTolerance, double nextDeparture) {
        return (arrivalTime + delayTolerance - nextDeparture);
    }

    /**
     * 
     * @param delayTolerance
     * @param nextDeparture
     * @param epsilon
     * @return 
     */
    public double calcArrivalDT(double delayTolerance, double nextDeparture, double epsilon) {
        return (nextDeparture + (epsilon * delayTolerance));
    }

    @Override
    public void setLinkError(int link) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSimulationOption(int option) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getSimulationOption() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setEventScheduler(EventScheduler es) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
