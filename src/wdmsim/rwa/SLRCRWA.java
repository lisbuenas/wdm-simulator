package wdmsim.rwa;

import wdmsim.util.WeightedGraph;
import wdmsim.util.Dijkstra;
import wdmsim.*;

import java.util.TreeSet;

/**
 *
 * @author andred
 */
public class SLRCRWA implements RWA {

    private PhysicalTopology pt;
    private VirtualTopology vt;
    private ControlPlaneForRWA cp;
    private WeightedGraph graph;
    private FixedAlternateShortestPathFirstFitRWA routingModule;
    private Tracer tr = Tracer.getTracerObject();
    private double arrivalTime;
    public int linkFailed;
    public Flow flow;
    public boolean canDegradeFlow = false;
    public int simulationOption;

    private EventScheduler es;

    private MyStatistics st;

    /**
     * Testing modes 0 - Baseline 1 - Degraded 2 - Degraded + Rescheduled
     */
    private int mode = 0;

    @Override
    public void simulationInterface(PhysicalTopology pt, VirtualTopology vt, ControlPlaneForRWA cp) {
        this.pt = pt;
        this.vt = vt;
        this.cp = cp;
        this.graph = pt.getWeightedGraph();

        this.simulationOption = cp.getSimulationOption();

        routingModule = new FixedAlternateShortestPathFirstFitRWA();
        routingModule.simulationInterface(pt, vt, cp);
        routingModule.setParameters(5, 5, 2);
    }

    @Override
    public void flowArrival(Flow flowInfo) {
        long flowID;
        int src, dst, bw;
        int[] nodesaux;
        WeightedGraph g;
        LightPath lp;
        LightPath[] lps;
        boolean accepted;

        flowID = flowInfo.getID();
        src = flowInfo.getSource();
        dst = flowInfo.getDestination();
        bw = flowInfo.getRate();
        this.arrivalTime = flowInfo.getArrivalTime();
        
        st = MyStatistics.getMyStatisticsObject();
        /**
         * Baseline
         */

        g = new WeightedGraph(pt.getNumNodes());

        for (int i = 0; i < g.size(); i++) {
            for (int j = 0; j < g.size(); j++) {
                if (i != j && i != dst && j != src) {
                    lp = getCheaperLightpath(i, j, bw);
                    if (lp != null) {
                        g.addEdge(i, j, getLightpathCost(lp));
                    }
                }
            }
        }

        // 3- Calcula o menor caminho no grafo auxiliar;
        nodesaux = Dijkstra.getShortestPath(g, src, dst);
        if (nodesaux.length > 1) {
            // 4- Implementa o novo caminho na rede;
            lps = new LightPath[nodesaux.length - 1];
            accepted = true;
            for (int i = 0; i < nodesaux.length - 1; i++) {
                lp = implementCheaperLightpath(nodesaux[i], nodesaux[i + 1], bw);
                if (lp != null) {
                    lps[i] = lp;
                } else {
                    accepted = false;
                    break;
                }
            }
            if (accepted) {
                tr.addRounds(1);
                cp.acceptFlow(flowID, lps);
                return;
            }
        }
        
        if (simulationOption == 0){
            cp.blockFlow(flowID);
            return;
        }
            
        /**
         * End Baseline
         */
        
        if((simulationOption == 1 || simulationOption == 2) && !flowInfo.canDegrade()){
            cp.blockFlow(flowID);
            flowInfo.setDegraded();
            return;
        }
        
        /**
         * Degraded
         */
        bw /= 2;
        for (int i = 0; i < g.size(); i++) {
            for (int j = 0; j < g.size(); j++) {
                if (i != j && i != dst && j != src) {
                    lp = getCheaperLightpath(i, j, bw);
                    if (lp != null) {
                        g.addEdge(i, j, getLightpathCost(lp));
                    }
                }
            }
        }

        // 3- Calcula o menor caminho no grafo auxiliar;
        nodesaux = Dijkstra.getShortestPath(g, src, dst);
        if (nodesaux.length > 1) {
            // 4- Implementa o novo caminho na rede;
            lps = new LightPath[nodesaux.length - 1];
            accepted = true;
            for (int i = 0; i < nodesaux.length - 1; i++) {
                lp = implementCheaperLightpath(nodesaux[i], nodesaux[i + 1], bw);
                if (lp != null) {
                    lps[i] = lp;
                } else {
                    accepted = false;
                    break;
                }
            }
            if (accepted) {
                tr.addRounds(1);
                cp.acceptFlow(flowID, lps);
                st.acceptedDegraded();
                return;
            }
        }
        
        if(simulationOption != 2){
            cp.blockFlow(flowID);
            return;
        }
        
        st.acceptedRescheduled();
        /**
         * End Degraded
         */
        return;
    }

    @Override
    public void flowDeparture(long id) {
        //strategy to increase time
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath implementCheaperLightpath(int src, int dst, int bw) {
        long id;
        LightPath newLp, groomLp, lp;

        groomLp = getCheaperGroomingLightpath(src, dst, bw);
        newLp = getPossibleNewLightpath(src, dst, bw);
        if (groomLp != null || newLp != null) {
            if (groomLp != null && newLp != null) {
                if (compareLightpaths(groomLp, newLp) == 1) {
                    lp = newLp;
                    id = vt.createLightpath(lp.getLinks(), lp.getWavelengths(), 0, this.arrivalTime);
                    lp = vt.getLightpath(id);
                } else {
                    lp = groomLp;
                }

            } else if (groomLp != null) {
                lp = groomLp;
            } else {
                lp = newLp;
                id = vt.createLightpath(lp.getLinks(), lp.getWavelengths(), 0, this.arrivalTime);
                lp = vt.getLightpath(id);
            }
            return lp;
        }
        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath getCheaperLightpath(int src, int dst, int bw) {
        LightPath newLp, groomLp, lp;

        groomLp = getCheaperGroomingLightpath(src, dst, bw);

        try {
            for (int i = 0; i < groomLp.links.length; i++) {
                if (linkFailed == groomLp.links[i]) {
                    groomLp = null;//identify error on link
                }
            }
        } catch (Exception ec) {

        }
        newLp = getPossibleNewLightpath(src, dst, bw);
        //boolean containLinkFailed = true;
        try {
            for (int i = 0; i < newLp.links.length; i++) {
                if (linkFailed == groomLp.links[i]) {
                    //containLinkFailed = true;
                    groomLp = null;
                }
            }
        } catch (Exception ec) {

        }

        if (groomLp != null || newLp != null) {
            if (groomLp != null && newLp != null) {
                if (compareLightpaths(groomLp, newLp) == 1) {
                    lp = newLp;
                } else {
                    lp = groomLp;
                }

            } else if (groomLp != null) {
                lp = groomLp;
            } else {
                lp = newLp;
            }
            return lp;
        }
        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath getCheaperGroomingLightpath(int src, int dst, int bw) {
        LightPath lbest = null;
        boolean containLinkFailed;
        if (vt.hasLightpath(src, dst)) {
            TreeSet<LightPath> lps = vt.getLightpaths(src, dst);
            for (LightPath lp : lps) {
                containLinkFailed = false;

                for (int i = 0; i < lp.links.length; i++) {//remove lightpath with link
                    if (lp.links[i] == linkFailed) {
                        containLinkFailed = true;
                        lbest = null;
                    }
                }

                if (!containLinkFailed) {

                    if (vt.getLightpathBWAvailable(lp.getID()) >= bw) {
                        if (lbest == null) {
                            lbest = lp;
                        } else if (compareLightpaths(lbest, lp) == 1) {
                            lbest = lp;
                        }

                    } else if (vt.getLightpathBWAvailable(lp.getID()) >= bw * 0.5) {//with can be degraded
                        if (lbest == null) {
                            lbest = lp;
                        } else if (compareLightpaths(lbest, lp) == 1) {
                            lbest.degraded = true;
                            lbest = lp;
                        }
                    }
                }
            }

            return lbest;
        }

        return null;
    }

    /**
     *
     * @param src
     * @param dst
     * @param bw
     * @return
     */
    private LightPath getPossibleNewLightpath(int src, int dst, int bw) {
        LightPath lp = null;

        lp = routingModule.getPossibleNewLightpath(src, dst, bw);

        return lp;
    }

    /**
     *
     * @param lp
     * @return
     */
    private double getLightpathCost(LightPath lp) {
        // Weights
        double alpha = 1;
        double beta = 0.1;
        double gama = 0.001;

        double numWavelengths = (double) vt.hopCount(lp);
        double numGroomingPortPairs = 1;
        double numWavelengthConverters = (double) vt.usedConverters(lp);

        return (alpha * numWavelengths) + (beta * numGroomingPortPairs) + (gama * numWavelengthConverters);
    }

    /**
     *
     * @param lp1
     * @param lp2
     * @return
     */
    private int compareLightpaths(LightPath lp1, LightPath lp2) {

        double totalCost1, totalCost2;

        totalCost1 = getLightpathCost(lp1);
        totalCost2 = getLightpathCost(lp2);

        if (totalCost1 < totalCost1) {
            return -1;
        }

        if (totalCost1 > totalCost1) {
            return 1;
        }

        return 0;
    }

    /**
     *
     * @param lps
     * @return
     */
    private double getPathCost(LightPath[] lps) {
        double totalCost = 0;

        for (int i = 0; i
                < lps.length; i++) {
            totalCost += getLightpathCost(lps[i]);
        }

        return totalCost;
    }

    /**
     *
     * @param lps1
     * @param lps2
     * @return
     */
    private int comparePaths(LightPath[] lps1, LightPath[] lps2) {

        double totalCost1, totalCost2;

        totalCost1 = getPathCost(lps1);
        totalCost2 = getPathCost(lps2);

        if (totalCost1 < totalCost1) {
            return -1;
        }

        if (totalCost1 > totalCost1) {
            return 1;
        }

        return 0;
    }

    /**
     *
     * @param link
     */
    public void setLinkError(int link) {
        this.linkFailed = link;
    }

    @Override
    public void setSimulationOption(int option) {
        this.simulationOption = option;
    }

    @Override
    public int getSimulationOption() {
        return this.mode;
    }

    public void acceptedDegraded(Flow flow) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setEventScheduler(EventScheduler es) {
        this.es = es;
    }
}
