package wdmsim;

import DataView.DataAnalisys;
import DataView.GraphResults;
import org.jfree.ui.RefineryUtilities;

/**
 * The Main class takes care of the execution of the simulator, which includes
 * dealing with the arguments called (or not) on the command line.
 *
 * @author andred
 */
public class Main {

    /**
     * Instantiates a Simulator object and takes the arguments from the command
     * line. Based on the number of arguments, can detect if there are too many
     * or too few, which prints a message teaching how to run WDMSim. If the
     * number is correct, detects which arguments were applied and makes sure
     * they have the expected effect.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Simulator wdm;
        String usage = "Usage: WDMSim simulation_file seed [-trace] [-verbose] [minload maxload step]";
        String simConfigFile;
        boolean verbose = true;
        boolean trace = true;
        int seed = 1;
        double minload = 0, maxload = 0, step = 1;

        String[] args1 = {"teste5.xml", "1", "30", "120", "10"};//simple test to pass arguments
        //String[] args1 = { "teste5.xml", "1" ,"1","10","1"};//test load
        //String[] args1 = { "teste4.xml", "1" ,"50","400","50"};//simple test to pass arguments

        //10 amostras para intervalo de confiança e média
        args = args1;//pass inner arguments to test

        if (args.length < 2 || args.length > 7) {
            System.out.println(usage);
            System.exit(0);
        } else {
            if (args.length == 3 || args.length == 6) {
                if (args[2].equals("-verbose")) {
                    verbose = true;
                } else {
                    if (args[2].equals("-trace")) {
                        trace = true;
                    } else {
                        System.out.println(usage);
                        System.exit(0);
                    }
                }
            }

            if (args.length == 4 || args.length == 7) {
                if ((args[2].equals("-trace") && args[3].equals("-verbose")) || (args[3].equals("-trace") && args[2].equals("-verbose"))) {
                    trace = true;
                    verbose = true;
                } else {
                    System.out.println(usage);
                    System.exit(0);
                }
            }

            //trace = true;
            //verbose = true;
            if (args.length == 5 || args.length == 6 || args.length == 7) {
                minload = Double.parseDouble(args[args.length - 3]);
                maxload = Double.parseDouble(args[args.length - 2]);
                step = Double.parseDouble(args[args.length - 1]);
            }
        }

        simConfigFile = args[0];
        seed = Integer.parseInt(args[1]);
        
        DataAnalisys[] da = new DataAnalisys[3];// chart 1 BBR
        //size according iterations
        int totalIterations = 10;
        da[0] = new DataAnalisys();
        da[0].createDataset(totalIterations, 10);
        
        da[1] = new DataAnalisys();
        da[1].createDataset(totalIterations, 10);
        
        da[2] = new DataAnalisys();
        da[2].createDataset(totalIterations, 10);
        
        
        DataAnalisys[] da2 = new DataAnalisys[3];// chart 2 Degraded Services
        da2[0] = new DataAnalisys();
        da2[0].createDataset(totalIterations, 10);
        
        da2[1] = new DataAnalisys();
        da2[1].createDataset(totalIterations, 10);
        
        da2[2] = new DataAnalisys();
        da2[2].createDataset(totalIterations, 10);
        
        DataAnalisys[] da3 = new DataAnalisys[3];// chart 2 Degraded Services
        da3[0] = new DataAnalisys();
        da3[0].createDataset(totalIterations, 10);
        
        da3[1] = new DataAnalisys();
        da3[1].createDataset(totalIterations, 10);
        
        da3[2] = new DataAnalisys();
        da3[2].createDataset(totalIterations, 10);
        
        //running tests
        long startTime = System.currentTimeMillis();//measure
        
        int position = 0; //position on dataset
        int stepOffset = 30;
        
        //da[0] = new DataAnalisys[3];// instance the data element with the 3 graph dataset
        //da[1] = new DataAnalisys[3];// instance the data element with the 3 graph dataset
        //da[2] = new DataAnalisys[3];// instance the data element with the 3 graph dataset
        
        //for(int j = 0; j < totalIterations; j++){// run around the iterations (5 for testing)
            for(int i = 0; i < 3; i ++){
                position = 0; //reset condition
                for (double load = minload; load <= maxload; load += step) {
                    wdm = new Simulator();
                    //wdm.setGraph(chart,chart2,chart3);
                    //wdm.setDataAnalisys(da[i][]);

                    //wdm.setDataAnalisys(da[i], da2[i], da3[i]);
                    wdm.setDataAnalisys(da[i], da2[i], da3[i]);
                    //wdm.setMode(i);
                    wdm.setMode(i);
                    //wdm.setMode(2);
                    wdm.setPosition(position);
                    //wdm.setIteration(j);
                    wdm.setIteration(0);
                    wdm.Execute(simConfigFile, trace, verbose, load, seed);
                    //get the value
                    position++;
                }
            }
        //}
        
        System.out.println("Print Resultados!");
        da[0].printDataset();//testing
        da[1].printDataset();//testing
        da[2].printDataset();//testing
        
        //int test = 0;
        long endTime = System.currentTimeMillis();
        System.out.println("That took " + ((endTime - startTime)/ 1000)/60 + "  minutes");
        
        float dados[] =  da[0].average(da[0].getDataSet());
        float dados_2[] =  da2[0].average(da2[0].getDataSet());
        float dados_3[] =  da3[0].average(da3[0].getDataSet());
        
        /*float dados2[] =  da[1].average(da[1].getDataSet());
        float dados2_2[] =  da2[1].average(da2[1].getDataSet());
        float dados2_3[] =  da3[1].average(da3[1].getDataSet());
        
        float dados3[] =  da[2].average(da[2].getDataSet());
        float dados3_2[] =  da2[2].average(da2[2].getDataSet());
        float dados3_3[] =  da3[2].average(da3[2].getDataSet());*/
        
        float deviation = da[0].standardDeviation(da[0].getDataSet());
        //generate charts
        
        System.out.println("Standard deviation: "+ deviation);
        
        GraphResults chart = new GraphResults("Simulation results", "BBR");
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
        
        for(int i = 0 ; i < da[0].getDataSet().length; i++){
            chart.addDado(stepOffset + (int)(i*step),da[0].getData(i, 0), 0);
            chart.addDado(stepOffset + (int)(i*step),da[1].getData(i, 0), 1);
            chart.addDado(stepOffset + (int)(i*step),da[2].getData(i, 0), 2);
            //chart.addDado(stepOffset + (int)(i*step),da2[0].getData(i, 0), 1);
            //chart.addDado(stepOffset + (int)(i*step),da3[0].getData(i, 0), 2);
        }
        /*
        for(int i = 0; i < dados.length; i++){
            chart.addDado(stepOffset + (int)(i*step),dados[i], 0);
            chart.addDado(stepOffset + (int)(i*step),dados_2[i], 1);
            chart.addDado(stepOffset + (int)(i*step),dados_3[i], 2);
        }*/
        
        /*GraphResults chart2 = new GraphResults("Simulation results", "Degraded Services");
        chart2.pack();
        RefineryUtilities.centerFrameOnScreen(chart2);
        chart2.setVisible(true);
        
        for(int i = 0; i < dados2.length; i++){
            chart2.addDado(stepOffset + (int)(i*step),dados2[i], 0);
            chart2.addDado(stepOffset + (int)(i*step),dados2_2[i], 1);
            chart2.addDado(stepOffset + (int)(i*step),dados2_3[i], 2);
        }
        
        GraphResults chart3 = new GraphResults("Simulation results", "Rescheduled");
        chart3.pack();
        RefineryUtilities.centerFrameOnScreen(chart3);
        chart3.setVisible(true);
        
        for(int i = 0; i < dados3.length; i++){
            chart3.addDado(stepOffset + (int)(i*step),dados3[i], 0);
            chart3.addDado(stepOffset + (int)(i*step),dados3_2[i], 1);
            chart3.addDado(stepOffset + (int)(i*step),dados3_3[i], 2);
        }*/
        
        /*
        GraphResults chart2 = new GraphResults("Simulation results", "Avg. Restorability");
        chart2.pack();
        RefineryUtilities.centerFrameOnScreen(chart2);
        chart2.setVisible(true);
        
        GraphResults chart3 = new GraphResults("Simulation results", "% of Degraded services");
        chart3.pack();
        RefineryUtilities.centerFrameOnScreen(chart3);
        chart3.setVisible(true);*/
    }
}