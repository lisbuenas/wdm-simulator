/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import wdmsim.rwa.ControlPlaneForRWA;
import wdmsim.rwa.RWA;

/**
 * The Control Plane is responsible for managing resources and connection within
 * the network.
 */
public class ControlPlane implements ControlPlaneForRWA {

    private RWA rwa;
    private PhysicalTopology pt;
    private VirtualTopology vt;
    private Map<Flow, Path> mappedFlows; // Flows that have been accepted into the network
    private Map<Long, Flow> activeFlows; // Flows that have been accepted or that are waiting for a decision 
    private Tracer tr = Tracer.getTracerObject();
    private MyStatistics st = MyStatistics.getMyStatisticsObject();
    private int simulationOption; //the type of the simulator

    private Event eventInput; // last event entry

    private Map<Long, Event> mapDepartures = new HashMap<Long, Event>();
    private EventScheduler queueDepartures = new EventScheduler(); //achar nextDeparture

    private EventScheduler events;

    /**
     * Creates a new ControlPlane object.
     *
     * @param rwaModule the name of the RWA class
     * @param pt the network's physical topology
     * @param vt the network's virtual topology
     */
    public ControlPlane(String rwaModule, PhysicalTopology pt, VirtualTopology vt, EventScheduler events) {
        Class RWAClass;
        
        mappedFlows = new HashMap<Flow, Path>();
        activeFlows = new HashMap<Long, Flow>();

        this.pt = pt;
        this.vt = vt;

        this.events = events;

        try {
            RWAClass = Class.forName(rwaModule);
            rwa = (RWA) RWAClass.newInstance();
            rwa.simulationInterface(pt, vt, this);
            rwa.setSimulationOption(simulationOption);//set option
            rwa.setEventScheduler(events);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     * Deals with an Event from the event queue. If it is of the
     * FlowArrivalEvent kind, adds it to the list of active flows. If it is from
     * the FlowDepartureEvent, removes it from the list.
     *
     * @param event the Event object taken from the queue
     */
    public void newEvent(Event event) {

        this.eventInput = event;
        
        //events = allEvents; // to re-schedul a demand
        if (event instanceof FlowArrivalEvent) {
            newFlow(((FlowArrivalEvent) event).getFlow());
            rwa.flowArrival(((FlowArrivalEvent) event).getFlow());
            //if(1 < 0){
                //update data
                //((FlowArrivalEvent) event).setTime(0);//update the current time
                //events.addEvent( (FlowArrivalEvent) event);//reintroduce element on scheduler
            //}
            //process DC resource
        } else if (event instanceof FlowDepartureEvent) {
            removeFlow(((FlowDepartureEvent) event).getID());
            rwa.flowDeparture(((FlowDepartureEvent) event).getID());
        } else if (event instanceof FailureEvent) {
            ((FailureEvent) event).createFailure(vt);
            rwa.setLinkError(vt.errorLink);
        } else if(event instanceof FailureRestoreEvent)
            ((FailureRestoreEvent) event).restoreLink(vt);
    }

    /**
     * Adds a given active Flow object to a determined Physical Topology.
     *
     * @param id unique identifier of the Flow object
     * @param lightpaths the Path, or list of LighPath objects
     * @return true if operation was successful, or false if a problem occurred
     */
    public boolean acceptFlow(long id, LightPath[] lightpaths) {
        Flow flow;

        if (id < 0 || lightpaths.length < 1) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return false;
            }
            flow = activeFlows.get(id);
            if (!canAddFlowToPT(flow, lightpaths)) {
                return false;
            }
            addFlowToPT(flow, lightpaths);
            mappedFlows.put(flow, new Path(lightpaths));
            tr.acceptFlow(flow, lightpaths);
            st.acceptFlow(flow, lightpaths);
            return true;
        }
    }

    /**
     * Removes a given Flow object from the list of active flows.
     *
     * @param id unique identifier of the Flow object
     * @return true if operation was successful, or false if a problem occurred
     */
    public boolean blockFlow(long id) {
        Flow flow;

        if (id < 0) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return false;
            }
            flow = activeFlows.get(id);
            if (mappedFlows.containsKey(flow)) {
                return false;
            }
            activeFlows.remove(id);
            tr.blockFlow(flow);
            st.blockFlow(flow);
            return true;
        }
    }

    /**
     * Removes a given Flow object from the Physical Topology and then puts it
     * back, but with a new route (set of LightPath objects).
     *
     * @param id unique identifier of the Flow object
     * @param lightpaths list of LightPath objects, which form a Path
     * @return true if operation was successful, or false if a problem occurred
     */
    public boolean rerouteFlow(long id, LightPath[] lightpaths) {
        Flow flow;
        Path oldPath;

        if (id < 0 || lightpaths.length < 1) {
            throw (new IllegalArgumentException());
        } else {
            if (!activeFlows.containsKey(id)) {
                return false;
            }
            flow = activeFlows.get(id);
            if (!mappedFlows.containsKey(flow)) {
                return false;
            }
            oldPath = mappedFlows.get(flow);
            removeFlowFromPT(flow, lightpaths);
            if (!canAddFlowToPT(flow, lightpaths)) {
                addFlowToPT(flow, oldPath.getLightpaths());
                return false;
            }
            addFlowToPT(flow, lightpaths);
            mappedFlows.put(flow, new Path(lightpaths));
            //tr.flowRequest(id, true);
            return true;
        }
    }

    /**
     * Adds a given Flow object to the HashMap of active flows. The HashMap also
     * stores the object's unique identifier (ID).
     *
     * @param flow Flow object to be added
     */
    private void newFlow(Flow flow) {
        activeFlows.put(flow.getID(), flow);
    }

    /**
     * Removes a given Flow object from the list of active flows.
     *
     * @param id the unique identifier of the Flow to be removed
     */
    private void removeFlow(long id) {
        Flow flow;
        LightPath[] lightpaths;

        if (activeFlows.containsKey(id)) {
            flow = activeFlows.get(id);
            if (mappedFlows.containsKey(flow)) {
                lightpaths = mappedFlows.get(flow).getLightpaths();
                removeFlowFromPT(flow, lightpaths);
                mappedFlows.remove(flow);
            }
            activeFlows.remove(id);
        }
    }

    /**
     * Removes a given Flow object from a Physical Topology.
     *
     * @param flow the Flow object that will be removed from the PT
     * @param lightpaths a list of LighPath objects
     */
    private void removeFlowFromPT(Flow flow, LightPath[] lightpaths) {
        int[] links;
        int[] wvls;

        for (int i = 0; i < lightpaths.length; i++) {
            links = lightpaths[i].getLinks();
            wvls = lightpaths[i].getWavelengths();
            for (int j = 0; j < links.length; j++) {
                pt.getLink(links[j]).removeTraffic(wvls[j], flow.getRate());
            }
            // Can the lightpath be removed?
            if (vt.isLightpathIdle(lightpaths[i].getID())) {
                vt.removeLightPath(lightpaths[i].getID());
            }
        }

    }

    /**
     * Says whether or not a given Flow object can be added to a determined
     * Physical Topology, based on the amount of bandwidth the flow requires
     * opposed to the available bandwidth.
     *
     * @param flow the Flow object to be added
     * @param lightpaths list of LightPath objects the flow uses
     * @return true if Flow object can be added to the PT, or false if it can't
     */
    private boolean canAddFlowToPT(Flow flow, LightPath[] lightpaths) {
        int[] links;
        int[] wvls;

        // Test the availability of resources
        for (int i = 0; i < lightpaths.length; i++) {
            links = lightpaths[i].getLinks();
            wvls = lightpaths[i].getWavelengths();
            for (int j = 0; j < links.length; j++) {
                if (pt.getLink(links[j]).amountBWAvailable(wvls[j]) < flow.getRate()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Adds a Flow object to a Physical Topology. This means adding the flow to
     * the network's traffic, which simply decreases the available bandwidth.
     *
     * @param flow the Flow object to be added
     * @param lightpaths list of LightPath objects the flow uses
     */
    private void addFlowToPT(Flow flow, LightPath[] lightpaths) {
        int[] links;
        int[] wvls;

        // Implements it
        for (int i = 0; i < lightpaths.length; i++) {
            links = lightpaths[i].getLinks();
            wvls = lightpaths[i].getWavelengths();
            for (int j = 0; j < links.length; j++) {
                //
                pt.getLink(links[j]).addTraffic(wvls[j], flow.getRate());
                //map the services
                
                //flow.getID();
            }
        }
    }

    /**
     * Retrieves a Path object, based on a given Flow object. That's possible
     * thanks to the HashMap mappedFlows, which maps a Flow to a Path.
     *
     * @param flow Flow object that will be used to find the Path object
     * @return Path object mapped to the given flow
     */
    public Path getPath(Flow flow) {
        return mappedFlows.get(flow);
    }

    /**
     * Retrieves the complete set of Flow/Path pairs listed on the mappedFlows
     * HashMap.
     *
     * @return the mappedFlows HashMap
     */
    public Map<Flow, Path> getMappedFlows() {
        return mappedFlows;
    }

    /**
     * Retrieves a Flow object from the list of active flows.
     *
     * @param id the unique identifier of the Flow object
     * @return the required Flow object
     */
    public Flow getFlow(long id) {
        return activeFlows.get(id);
    }

    /**
     * Counts number of times a given LightPath object is used within the Flow
     * objects of the network.
     *
     * @param id unique identifier of the LightPath object
     * @return integer with the number of times the given LightPath object is
     * used
     */
    public int getLightpathFlowCount(long id) {
        int num = 0;
        Path p;
        LightPath[] lps;
        ArrayList<Path> ps = new ArrayList<Path>(mappedFlows.values());
        for (int i = 0; i < ps.size(); i++) {
            p = ps.get(i);
            lps = p.getLightpaths();
            for (int j = 0; j < lps.length; j++) {
                if (lps[j].getID() == id) {
                    num++;
                    break;
                }
            }
        }
        return num;
    }
    
    //return services on current flow

    /**
     *
     * @param id
     * @param delayedArrival
     * @param newDepartureTime
     * @param newDelayTolerance
     */
    public void reScheduleEvent(long id, double delayedArrival, double newDepartureTime, double newDelayTolerance) {
        Event eventDeparture;

        /*updateFlowTime(id, delayedArrival, newDelayTolerance);

        //modify and reSchelul the arrivalEvent        
        events.removeEvent(eventInput);
        eventInput.setTime(delayedArrival);
        //eventInput.setTime(0);
        events.addEvent(eventInput);

        //modify and reSchelul the departureEvent
        eventDeparture = mapDepartures.get(id);
        events.removeEvent(eventDeparture);
        queueDepartures.removeEvent(eventDeparture);
        mapDepartures.remove(id);

        eventDeparture.setTime(newDepartureTime);
        events.addEvent(eventDeparture);
        queueDepartures.addEvent(eventDeparture);
        mapDepartures.put(id, eventDeparture);
        tr.addExtraArrival();*/
        
        events.getEvent().setTime(newDepartureTime);
    }

    /**
     *
     * @param id
     * @param delayedArrival
     * @param newDelayTolerance
     */
    private void updateFlowTime(long id, double delayedArrival, double newDelayTolerance) {
        Flow flow;
        flow = getFlow(id);
        flow.updateArrivalTime(delayedArrival);
        flow.updateDelayTolerance(newDelayTolerance);
        flow.updateRounds();
    }

    /**
     *
     * @return
     */
    public double nextDeparture() {
        return (queueDepartures.seekNextEvent()).getTime();
    }
    
    /**
     * 
     */
    public void addRescheduled(){
        
    }
    
    /**
     * 
     */
    public void addDegraded(){
        
    }

    /**
     * Return the parameters of the simulation mode
     * 0 - Baseline
     * 1 - Degraded
     * 2 - Degraded with rescheduling
     * @return 
     */
    @Override
    public int getSimulationOption() {
        return simulationOption;
    }

    @Override
    public void setSimulationOption(int option) {
        this.simulationOption = option;
        rwa.setSimulationOption(simulationOption);//set option
    }
}