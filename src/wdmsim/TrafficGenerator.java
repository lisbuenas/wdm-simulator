/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim;

import java.util.ArrayList;
import java.util.List;
import wdmsim.util.Distribution;
import org.w3c.dom.*;

/**
 * Generates the network's traffic based on the information passed through the
 * command line arguments and the XML simulation file.
 *
 * @author andred
 */
public class TrafficGenerator {

    private int calls;
    private double load;
    double time;
    private int maxRate;
    private TrafficInfo[] callsTypesInfo;
    private double meanRate;
    private double meanHoldingTime;
    private int TotalWeight;
    private int numberCallsTypes;
    private int failures;
    private int[] DC;

    /**
     * Creates a new TrafficGenerator object. Extracts the traffic information
     * from the XML file and takes the chosen load and seed from the command
     * line arguments.
     *
     * @param xml file that contains all information about the simulation
     * @param forcedLoad range of offered loads for several simulations
     */
    public TrafficGenerator(Element xml, double forcedLoad) {
        int rate, cos, weight,canDegrade;
        double holdingTime;
        failures = 0;

        calls = Integer.parseInt(xml.getAttribute("calls"));
        load = forcedLoad;
        if (load == 0) {
            load = Double.parseDouble(xml.getAttribute("load"));
        }
        maxRate = Integer.parseInt(xml.getAttribute("max-rate"));

        if (Simulator.verbose) {
            System.out.println(xml.getAttribute("calls") + " calls, " + xml.getAttribute("load") + " erlangs.");
        }

        // Process calls
        NodeList callslist = xml.getElementsByTagName("calls");
        numberCallsTypes = callslist.getLength();
        if (Simulator.verbose) {
            System.out.println(Integer.toString(numberCallsTypes) + " type(s) of calls:");
        }

        callsTypesInfo = new TrafficInfo[numberCallsTypes];

        TotalWeight = 0;
        meanRate = 0;
        meanHoldingTime = 0;

        for (int i = 0; i < numberCallsTypes; i++) {
            TotalWeight += Integer.parseInt(((Element) callslist.item(i)).getAttribute("weight"));
        }

        for (int i = 0; i < numberCallsTypes; i++) {
            holdingTime = Double.parseDouble(((Element) callslist.item(i)).getAttribute("holding-time"));
            rate = Integer.parseInt(((Element) callslist.item(i)).getAttribute("rate"));
            cos = Integer.parseInt(((Element) callslist.item(i)).getAttribute("cos"));
            weight = Integer.parseInt(((Element) callslist.item(i)).getAttribute("weight"));
            canDegrade = Integer.parseInt(((Element) callslist.item(i)).getAttribute("degrade"));
            meanRate += (double) rate * ((double) weight / (double) TotalWeight);
            meanHoldingTime += holdingTime * ((double) weight / (double) TotalWeight);
            callsTypesInfo[i] = new TrafficInfo(holdingTime, rate, cos, weight);
            if(canDegrade == 1)
                callsTypesInfo[i].setDegrade();
            if (Simulator.verbose) {
                System.out.println("#################################");
                System.out.println("Weight: " + Integer.toString(weight) + ".");
                System.out.println("COS: " + Integer.toString(cos) + ".");
                System.out.println("Rate: " + Integer.toString(rate) + "Mbps.");
                System.out.println("Mean holding time: " + Double.toString(holdingTime) + " seconds.");
            }
        }
    }

    /**
     * Generates the network's traffic.
     *
     * @param events EventScheduler object that will contain the simulation
     * events
     * @param pt the network's Physical Topology
     * @param seed a number in the interval [1,25] that defines up to 25
     * different random simulations
     */
    public void generateTraffic(PhysicalTopology pt, EventScheduler events, int seed) {

        // Compute the weight vector
        int[] weightVector = new int[TotalWeight];
        int aux = 0;
        for (int i = 0; i < numberCallsTypes; i++) {
            for (int j = 0; j < callsTypesInfo[i].getWeight(); j++) {
                weightVector[aux] = i;
                aux++;
            }
        }

        /* Compute the arrival time
         *
         * load = meanArrivalRate x holdingTime x bw/maxRate
         * 1/meanArrivalRate = (holdingTime x bw/maxRate)/load
         * meanArrivalTime = (holdingTime x bw/maxRate)/load
         */
        double meanArrivalTime = (meanHoldingTime * (meanRate / (double) maxRate)) / load;

        // Generate events
        int type, src, dst;
        
        List<Integer> dstList = new ArrayList<Integer>();
        
        for(int i = 0; i < pt.dcVector.length; i++){
            if(pt.dcVector[i] != null)
                dstList.add(pt.dcVector[i].getID());
        }
        
        //double time = 0.0;
        time = 0.0;
        int id = 0;
        int numNodes = pt.getNumNodes();
        Distribution dist1, dist2, dist3, dist4;
        Event event;

        dist1 = new Distribution(1, seed);
        dist2 = new Distribution(2, seed);
        dist3 = new Distribution(3, seed);
        dist4 = new Distribution(4, seed);

        int failureCounter = 1000, failureAt = 0, stepFailure = 0, failureLink = 0, restoreCoef = 10;

        for (int j = 0; j < calls; j++) {

            type = weightVector[dist1.nextInt(TotalWeight)];
            src = dst = dist2.nextInt(numNodes);//including a DC
            
            while (src == dst) {
                //dst = dist2.nextInt(numNodes);
                dst = dstList.get((int)((dstList.size())*Math.random())); //sort between the available DCs
            }
            
            Flow f1 = new Flow(id, src, dst, callsTypesInfo[type].getRate(), 0, callsTypesInfo[type].getCOS());
            if(callsTypesInfo[type].getDegrade())
                f1.setDegrade();
            
            //event = new FlowArrivalEvent(new Flow(id, src, dst, callsTypesInfo[type].getRate(), 0, callsTypesInfo[type].getCOS()));
            event = new FlowArrivalEvent(f1);
            time += dist3.nextExponential(meanArrivalTime);
            event.setTime(time);
            events.addEvent(event);
            event = new FlowDepartureEvent(id);
            event.setTime(time + dist4.nextExponential(callsTypesInfo[type].getHoldingTime()));//appoint to measure
            events.addEvent(event);
            id++;

            if (failureCounter == 1000) {//generate failure event in fixed steps with random interval
                failureAt = (int) (Math.random() * 1000);
                
                failureAt = 1000*j + dist3.nextInt(1000);
                failureLink = (int) (pt.getNumLinks() * Math.random());//set failure at link
                //int failureNode =  pt.getNode(id); //next improvement
                failureAt += stepFailure;
                failureCounter = 0;
            }
            
            failureCounter++;

            if (id == failureAt) {//insert failure event on queue
                
                event = new FailureEvent(pt, pt.getLink(failureLink).getID(), 0);
                events.addEvent(event);//create failure event
                //event.setTime(pt.getLink(failureLink).getWeight() * restoreCoef);//link vector?
                event.setTime(10);//link vector?
                events.addEvent(event);
                
                event = new RestoreEvent(pt, pt.getLink(failureLink).getID(),pt.getLink(failureLink).getBandwidth());//restore failure
                events.addEvent(event);//add to scheduler

                stepFailure += 1000;
                failures++;
            }
        }
        System.out.println(time);
    }
}