
package wdmsim;

import java.util.ArrayList;
import wdmsim.util.WeightedGraph;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import org.w3c.dom.*;

/**
 * The virtual topology is created based on a given Physical Topology and
 * on the lightpaths specified on the XML file.
 * 
 * @author andred
 */
public class VirtualTopology {

    private long nextLightpathID = 0;
    private TreeSet<LightPath>[][] adjMatrix;
    private int adjMatrixSize;
    public int errorLink;
    protected Map<Long, LightPath> lightPaths;
    public ArrayList <int[]> linkContentLightPath;
    private PhysicalTopology pt;
    private Tracer tr = Tracer.getTracerObject();
    private MyStatistics st = MyStatistics.getMyStatisticsObject();
    
    private static class LightPathSort implements Comparator<LightPath> {

    	@Override
        public int compare(LightPath lp1, LightPath lp2) {
            if (lp1.getID() < lp2.getID()) {
                return -1;
            }
            if (lp1.getID() > lp2.getID()) {
                return 1;
            }
            return 0;
        }
    }
    
    /**
     * Creates a new VirtualTopology object.
     * 
     * @param xml file that contains all simulation information
     * @param pt Physical Topology of the network
     */
    @SuppressWarnings("unchecked")
    public VirtualTopology(Element xml, PhysicalTopology pt) {
        int nodes, lightpaths;

        lightPaths = new HashMap<Long, LightPath>();
        //lightPaths = new ConcurrentHashMap<Long, LightPath>();

        try {
            this.pt = pt;
            if (Simulator.verbose) {
                System.out.println(xml.getAttribute("name"));
            }

            adjMatrixSize = nodes = pt.getNumNodes();

            // Process lightpaths
            adjMatrix = new TreeSet[nodes][nodes];
            for (int i = 0; i < nodes; i++) {
                for (int j = 0; j < nodes; j++) {
                    if (i != j) {
                        adjMatrix[i][j] = new TreeSet<LightPath>(new LightPathSort());
                    }
                }
            }
            NodeList lightpathlist = xml.getElementsByTagName("lightpath");
            lightpaths = lightpathlist.getLength();
            if (Simulator.verbose) {
                System.out.println(Integer.toString(lightpaths) + " lightpath(s)");
            }
            if (lightpaths > 0) {
                //TODO
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    /**
     * First, creates a lightpath in the Physical Topology through the createLightpathInPT
     * function. Then, gets the lightpath's source and destination nodes, so a new 
     * LightPath object can finally be created and added to the lightPaths HashMap
     * and to the adjMatrix TreeSet. 
     * 
     * @param links list of integers that represent the links that form the lightpath
     * @param wavelengths list of wavelength values used in the lightpath links
     * @return -1 if LightPath object cannot be created, or its unique identifier otherwise
     */
    public long createLightpath(int[] links, int[] wavelengths, double timeLife, double lastUpdateOfLifeTime) {
        LightPath lp;
        int src, dst;
        long id;
        
        if (links.length < 1 || wavelengths.length != links.length) {
            throw (new IllegalArgumentException());
        } else {
            if (!canCreateLightpath(links, wavelengths)) {
                return -1;
            }
            
            createLightpathInPT(links, wavelengths);
            src = pt.getLink(links[0]).getSource();
            dst = pt.getLink(links[links.length - 1]).getDestination();
            id = this.nextLightpathID;
            lp = new LightPath(id, src, dst, links, wavelengths);

            //System.out.println(Integer.toString(links.length) + " " + Integer.toString(src) + " -> " + Integer.toString(dst));
            
            adjMatrix[src][dst].add(lp);
            lightPaths.put(nextLightpathID, lp);
            tr.createLightpath(lp);
            this.nextLightpathID++;
            
            return id;
        }
    }
    
    /**
     * First, removes a given lightpath in the Physical Topology through the removeLightpathInPT
     * function. Then, gets the lightpath's source and destination nodes, to remove it 
     * from the lightPaths HashMap and the adjMatrix TreeSet.
     * 
     * @param id the unique identifier of the lightpath to be removed
     * @return true if operation was successful, or false otherwise
     */
    public boolean removeLightPath(long id) {
        int src, dst;
        LightPath lp;

        if (id < 0) {
            throw (new IllegalArgumentException());
        } else {
            if (!lightPaths.containsKey(id)) {
                return false;
            }
            lp = lightPaths.get(id);
            removeLightpathFromPT(lp.getLinks(), lp.getWavelengths());
            src = lp.getSource();
            dst = lp.getDestination();

            lightPaths.remove(id);
            adjMatrix[src][dst].remove(lp);
            tr.removeLightpath(lp);

            return true;
        }
    }
    
    /**
     * Removes a given lightpath from the Physical Topology and then puts it back,
     * but with a new route (set of links).
     * 
     * @param id unique identifier of the lightpath to be rerouted
     * @param links list of integers that represent the links that form the lightpath
     * @param wavelengths list of wavelength values used in the lightpath links
     * @return true if operation was successful, or false otherwise
     */
    public boolean rerouteLightPath(long id, int[] links, int[] wavelengths) {
        int src, dst;
        LightPath old, lp;
        if (links.length < 1 || wavelengths.length != links.length) {
            throw (new IllegalArgumentException());
        } else {
            if (!lightPaths.containsKey(id)) {
                return false;
            }
            old = lightPaths.get(id);
            removeLightpathFromPT(old.getLinks(), old.getWavelengths());
            if (!canCreateLightpath(links, wavelengths)) {
                createLightpathInPT(old.getLinks(), old.getWavelengths());
                return false;
            }
            createLightpathInPT(links, wavelengths);
            src = pt.getLink(links[0]).getSource();
            dst = pt.getLink(links[links.length - 1]).getDestination();
            adjMatrix[src][dst].remove(old);
            lp = new LightPath(id, src, dst, links, wavelengths);
            adjMatrix[src][dst].add(lp);
            lightPaths.put(id, lp);
            return true;
        }
    }
    
    /**
     * Says whether or not a given LightPath object has a
     * determined amount of available bandwidth.
     * 
     * @param src the lightpath's source node
     * @param dst the lightpath's destination node
     * @param bw required amount of bandwidth
     * @return true if lightpath is available
     */
    public boolean isLightpathAvailable(int src, int dst, int bw) {
        TreeSet<LightPath> lps = getLightpaths(src, dst);

        for (LightPath lp : lps) {
            if (getLightpathBWAvailable(lp.getID()) >= bw) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Retrieves a TreeSet with the Virtual Topology's available lightpaths.
     * 
     * @param src the lightpath's source node
     * @param dst the lightpath's destination node
     * @param bw required amount of available bandwidth the lightpath must have
     * @return a TreeSet with the available lightpaths
     */
    public TreeSet<LightPath> getAvailableLightpaths(int src, int dst, int bw) {
        TreeSet<LightPath> lps = getLightpaths(src, dst);

        if (lps != null && !lps.isEmpty()) {
            Iterator<LightPath> it = lps.iterator();

            while (it.hasNext()) {
                if (getLightpathBWAvailable(it.next().getID()) < bw) {
                    it.remove();
                }
            }
            return lps;
        } else {
            return null;
        }
    }
    
    /**
     * Retrieves the available bandwidth of a given lightpath.
     * 
     * @param id the lightpath's unique identifier
     * @return amount of available bandwidth
     */
    public int getLightpathBWAvailable(long id) {
        int aux, bw = Integer.MAX_VALUE;

        int[] links, wvls;
        links = getLightpath(id).getLinks();
        wvls = getLightpath(id).getWavelengths();
        for (int i = 0; i < links.length; i++) {
            aux = pt.getLink(links[i]).amountBWAvailable(wvls[i]);
            if (aux < bw) {
                bw = aux;
            }
        }
        return bw;
    }
    
    /**
     * Says whether or not a given lightpath is idle, i.e.,
     * all its bandwidth is available.
     * 
     * @param id the lightpath's unique identifier
     * @return true if lightpath is idle, or false otherwise
     */
    public boolean isLightpathIdle(long id) {
        int[] links, wvls;
        links = getLightpath(id).getLinks();
        wvls = getLightpath(id).getWavelengths();
        return pt.getLink(links[0]).getBandwidth() - pt.getLink(links[0]).amountBWAvailable(wvls[0]) == 0;
    }
    
    /**
     * Says whether or not a given lightpath is full, i.e.,
     * all its bandwidth is allocated.
     * 
     * @param id the lightpath's unique identifier
     * @return true if lightpath is full, or false otherwise
     */
    public boolean isLightpathFull(long id) {
        int[] links, wvls;
        links = getLightpath(id).getLinks();
        wvls = getLightpath(id).getWavelengths();
        return pt.getLink(links[0]).amountBWAvailable(wvls[0]) == 0;
    }
    
    /**
     * Retrieves a determined LightPath object from the Virtual Topology.
     * 
     * @param id the lightpath's unique identifier
     * @return the required lightpath
     */
    public LightPath getLightpath(long id) {
        if (id < 0) {
            throw (new IllegalArgumentException());
        } else {
            if (lightPaths.containsKey(id)) {
                return lightPaths.get(id);
            } else {
                return null;
            }
        }
    }
    
    /**
     * 
     * @param id 
     */
    public void setErrorLink(int id){
       this.errorLink = id; 
    }
    
    public void resetErrorLink(){
        this.errorLink = -1;
    }
    
    /**
     * Retrieves the TreeSet with all LightPath objects that
     * belong to the Virtual Topology.
     * 
     * @param src the lightpath's source node
     * @param dst the lightpath's destination node
     * @return the TreeSet with all of the lightpaths
     */
    public TreeSet<LightPath> getLightpaths(int src, int dst) {
        return new TreeSet<LightPath>(adjMatrix[src][dst]);
    }
    
    /**
     * Retrieves the adjacency matrix of the Virtual Topology.
     * 
     * @return the VirtualTopology object's adjMatrix
     */
    public TreeSet<LightPath>[][] getAdjMatrix() {
        return adjMatrix;
    }
    
    /**
     * Says whether or not a lightpath exists, based only on its source
     * and destination nodes.
     * 
     * @param src the lightpath's source node
     * @param dst the lightpath's destination node
     * @return true if the lightpath exists, or false otherwise
     */
    public boolean hasLightpath(int src, int dst) {
        //System.out.println("hasLightpath"+Integer.toString(src)+" -> "+Integer.toString(dst));
        if (adjMatrix[src][dst] != null) {
            //System.out.println("Not null");
            if (!adjMatrix[src][dst].isEmpty()) {
                //System.out.println("Not empty");
                return true;
            }
        }
        return false;
    }
    
    /**
     * Says whether or not a lightpath can be created, based only on its
     * links and wavelengths.
     * 
     * @param links list of integers that represent the links that form the lightpath
     * @param wavelengths list of wavelength values used in the lightpath links
     * @return true if the lightpath can be created, or false otherwise
     */
    public boolean canCreateLightpath(int[] links, int[] wavelengths) {
        int wvl1, wvl2, d, src, dst;
        // Available wvl converters and range
        wvl1 = wavelengths[0];
        for (int i = 1; i < wavelengths.length; i++) {
            wvl2 = wavelengths[i];
            if (wvl1 != wvl2) { // If changed the wvl
                d = Math.max(wvl1, wvl2) - Math.min(wvl1, wvl2);
                src = pt.getLink(links[i]).getSource();
                if (!pt.getNode(src).hasFreeWvlConverters() || (d > pt.getNode(src).getWvlConversionRange())) {
                    return false;
                }
                if (i < wavelengths.length - 1) { // If it is not the last link 
                    dst = pt.getLink(links[i]).getDestination();
                    if (!pt.getNode(dst).hasFreeWvlConverters() || (d > pt.getNode(dst).getWvlConversionRange())) {
                        return false;
                    }
                }
                wvl1 = wvl2;
            }
        }
        // Available wvl transceivers
        if (!pt.getNode(pt.getLink(links[0]).getSource()).hasFreeGroomingInputPort()) {
            return false;
        }
        if (!pt.getNode(pt.getLink(links[links.length - 1]).getDestination()).hasFreeGroomingOutputPort()) {
            return false;
        }
        
        // Available wavelengths
        //for (int i = 0; i < links.length - (errorLink > 0? 1:0); i++) {
        for (int i = 0; i < links.length; i++) {
            if (!pt.getLink(links[i]).isWLAvailable(wavelengths[i])) {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * Reserves, in the physical topology, the resources a given lightpath needs:
     * links, wavelengths and wavelength converters (if necessary).
     * 
     * @param links list of integers that represent the links that form the lightpath 
     * @param wavelengths list of wavelength values used in the lightpath links
     */
    private void createLightpathInPT(int[] links, int[] wavelengths) {
        int wvl1, wvl2;
        // Reserve wvl converters
        wvl1 = wavelengths[0];
        for (int i = 1; i < wavelengths.length; i++) {
            wvl2 = wavelengths[i];
            if (wvl1 != wvl2) { // If changed the wvl
                pt.getNode(pt.getLink(links[i]).getSource()).reserveWvlConverter();
                if (i < wavelengths.length - 1) { // If it is not the last link 
                    pt.getNode(pt.getLink(links[i]).getDestination()).reserveWvlConverter();
                }
                wvl1 = wvl2;
            }
        }
        // Reserve ports
        pt.getNode(pt.getLink(links[0]).getSource()).reserveGroomingInputPort();
        pt.getNode(pt.getLink(links[links.length - 1]).getDestination()).reserveGroomingOutputPort();
        // Reserve wvls
        for (int i = 0; i < links.length; i++) {
            pt.getLink(links[i]).reserveWavelength(wavelengths[i]);
        }
    }
    
    /**
     * Releases, in the physical topology, the resources a given lightpath was using:
     * links, wavelengths and wavelength converters (if necessary).
     * 
     * @param links list of integers that represent the links that form the lightpath
     * @param wavelengths list of wavelength values used in the lightpath links
     */
    private void removeLightpathFromPT(int[] links, int[] wavelengths) {
        int wvl1, wvl2;
        // Release wvl converters
        wvl1 = wavelengths[0];
        for (int i = 1; i < wavelengths.length; i++) {
            wvl2 = wavelengths[i];
            if (wvl1 != wvl2) { // If changed the wvl
                pt.getNode(pt.getLink(links[i]).getSource()).releaseWvlConverter();
                if (i < wavelengths.length - 1) { // If it is not the last link 
                    pt.getNode(pt.getLink(links[i]).getDestination()).releaseWvlConverter();
                }
                wvl1 = wvl2;
            }
        }
        // Release ports
        pt.getNode(pt.getLink(links[0]).getSource()).releaseGroomingInputPort();
        pt.getNode(pt.getLink(links[links.length - 1]).getDestination()).releaseGroomingOutputPort();
        // Release wvls
        for (int i = 0; i < links.length; i++) {
            pt.getLink(links[i]).releaseWavelength(wavelengths[i]);
        }
    }
    
    /**
     * Retrieves the number of links (or hops) a given LightPath object has.
     * 
     * @param lp the LightPath object
     * @return the number of hops the lightpath has
     */
    public int hopCount(LightPath lp) {
        return lp.getLinks().length;
    }
    
    /**
     * Retrieves the number of wavelength converters a given LightPath object uses.
     * 
     * @param lp the LightPath object
     * @return the number of converters the lightpath uses
     */
    public int usedConverters(LightPath lp) {
        int[] wvls = lp.getWavelengths();
        int numConv = 0;
        int wvl = wvls[0];
        
        for (int i = 1; i < wvls.length; i++) {
            if (wvl != wvls[i]) {
                numConv++;
                wvl = wvls[i];
            }
        }
        return numConv;
    }

    /**
     * Returns a weighted graph with vertices representing the physical network
     * nodes, and the edges representing the physical links.
     * 
     * The weight of each edge receives the same value of the original link
     * weight if the wavelength wvl in that link has at least bw Mbps of
     * bandwidth available. Otherwise it has no edges. 
     * 
     * @param wvl   the wavelength id 
     * @param bw    the amount of bandwidth to be established
     * @return      an WeightedGraph class object 
     */
    public WeightedGraph getWeightedGraph(int wvl, int bw) {
        WDMLink link;
        int nodes = pt.getNumNodes();
        WeightedGraph g = new WeightedGraph(nodes);
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < nodes; j++) {
                if (pt.hasLink(i, j)) {
                    link = pt.getLink(i, j);
                    if (link.amountBWAvailable(wvl) >= bw) {
                        g.addEdge(i, j, link.getWeight());
                    }
                }
            }
        }
        return g;
    }
    
    /**
     * Retrieves the lightpaths of a weighted graph.
     * 
     * @param bw required amount of bandwidth
     * @return a weighted graph formed only by the lightpaths
     */
    public WeightedGraph getLightpathsGraph(int bw) {
        int nodes = pt.getNumNodes();
        WeightedGraph g = new WeightedGraph(nodes);
        for (int i = 0; i < nodes; i++) {
            for (int j = 0; j < nodes; j++) {
                if (i != j) {
                    if (getAvailableLightpaths(i, j, bw) != null) {
                        g.addEdge(i, j, 1);
                    }
                }
            }
        }
        return g;
    }
    
    public synchronized void removeLightPathWithLink(int linkError){
        //Available link
        /*for(int i = 0; i < links.length; i++){
            if(links[i] ==  errorLink)
                return false;
        }*/
        
    }
    
    /**
     * Prints all lightpaths belonging to the Virtual Topology.
     * 
     * @return string containing all the elements of the adjMatrix TreeSet
     */
    @Override
    public String toString() {
        String vtopo = "";
        for (int i = 0; i < adjMatrixSize; i++) {
            for (int j = 0; j < adjMatrixSize; j++) {
                if (adjMatrix[i][j] != null) {
                    if (!adjMatrix[i][j].isEmpty()) {
                        vtopo += adjMatrix[i][j].toString() + "\n\n";
                    }
                }
            }
        }
        return vtopo;
    }
}
