
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim;

/**
 * In an optical network, a lightpath is a clear optical path which may traverse
 * several links in the network.
 * It is also good to know that information transmitted through lightpaths does not
 * undergo any conversion to or from electrical form.
 * 
 * @author andred
 */
public class LightPath {

    private long id;
    private int src;
    private int dst;
    public int[] links;
    private int[] wavelengths;
    public boolean degraded = false;

    /**
     * Creates a new LightPath object.
     * 
     * @param id            unique identifier
     * @param src           source node
     * @param dst           destination node
     * @param links         fyberlinks list composing the path
     * @param wavelengths    wavelengths list used in fiberlinks
     */
    public LightPath(long id, int src, int dst, int[] links, int[] wavelengths) {
        if (id < 0 || src < 0 || dst < 0 || links.length < 1 || wavelengths.length != links.length) {
            throw (new IllegalArgumentException());
        } else {
            this.id = id;
            this.src = src;
            this.dst = dst;
            this.links = links;
            this.wavelengths = wavelengths;
        }
    }
    
    /**
     * Retrieves the unique identifier of a given LightPath.
     * 
     * @return the LightPath's id attribute
     */
    public long getID() {
        return id;
    }
    
    /**
     * Retrieves the source node of a given LightPath.
     * 
     * @return the LightPath's src attribute
     */
    public int getSource() {
        return src;
    }
    
    /**
     * Retrieves the destination node of a given LightPath.
     * 
     * @return the LightPath's dst attribute.
     */
    public int getDestination() {
        return dst;
    }
    
    /**
     * Retrieves the LightPath's vector containing the identifier numbers
     * of the links that compose the path.
     * 
     * @return a vector of integers that represent fiberlinks identifiers
     */
    public int[] getLinks() {
        return links;
    }
    
    /**
     * Retrieves the LightPath's vector containing the wavelengths that
     * the fiber links in the path use.
     * 
     * @return a vector of integer that represent wavelengths
     */
    public int[] getWavelengths() {
        return wavelengths;
    }
    
    /**
     * The fiber links are physical hops. Therefore, by retrieving the number
     * of elements in a LightPath's list of fiber links, we get the number of
     * hops the LightPath has.
     * 
     * @return the number of hops in a given LightPath
     */
    public int getHops() {
        return links.length;
    }
    
    
    public void setDegraded(){
        this.degraded = true;
    }
    
    public boolean hasDegraded(){
        return this.degraded;
    }
    
    /**
     * Prints all information related to a given LightPath, starting with
     * its ID, to make it easier to identify.
     * 
     * @return string containing all the values of the LightPath's parameters
     */
    @Override
    public String toString() {
        String lightpath = Long.toString(id) + " " + Integer.toString(src) + " " + Integer.toString(dst) + " ";
        for (int i = 0; i < links.length; i++) {
            lightpath += Integer.toString(links[i]) + " (" + Integer.toString(wavelengths[i]) + ") ";
        }
        return lightpath;
    }
    
    public String toTrace() {
        String lightpath = Long.toString(id) + " " + Integer.toString(src) + " " + Integer.toString(dst) + " ";
        for (int i = 0; i < links.length; i++) {
            lightpath += Integer.toString(links[i]) + "_" + Integer.toString(wavelengths[i]) + " ";
        }
        return lightpath;
    }
}