/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author felis
 */
public class FailureEvent extends Event {

    //private Flow flow;
    private int linkId;
    private PhysicalTopology pt;
    private VirtualTopology vt;

    /**
     *
     * @param flow flow element
     * @param timeout time to failure happen
     */
    public FailureEvent(PhysicalTopology pt, int linkId, double timeout) {
        this.linkId = linkId;
        this.pt = pt;
    }

    /**
     *
     * @param numberOfSimultaneousFailures allow multiple failures
     */
    public void createFailure(VirtualTopology vt) {
        
        vt.setErrorLink(linkId);        
        
        for (int i = 0; i < pt.getNumWavelengths(); i++) {
            pt.getLink(linkId).availableBandwidth[i] = 0;
        }
    }
}
