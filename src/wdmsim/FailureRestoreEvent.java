/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim;

/**
 *
 * @author felis
 */
public class FailureRestoreEvent extends Event{
    private int linkId;
    private PhysicalTopology pt;
    private int bw;
    
    public FailureRestoreEvent(PhysicalTopology pt, int linkId, int bw) {
        this.linkId = linkId;
        this.pt = pt;
        this.bw = bw;
    }
    
    public void restoreLink(VirtualTopology vt){
        vt.resetErrorLink();
        for(int i = 0; i < pt.getLink(linkId).getWavelengths(); i ++)
            pt.getLink(linkId).availableBandwidth[i] = bw;
    }
}