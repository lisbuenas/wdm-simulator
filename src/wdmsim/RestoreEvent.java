/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wdmsim;

/**
 *
 * @author felis
 */
public class RestoreEvent extends Event {

    private int linkId;
    private int bw;
    private PhysicalTopology pt;

    /**
     *
     * @param pt the Physical Topology
     * @param linkId identifier of the link
     */
    public RestoreEvent(PhysicalTopology pt, int linkId, int  bw) {
        this.pt = pt;
        this.linkId = linkId;
        this.bw = bw;
    }

    /**
     *
     * @param bw the bandwith to be restored on link
     */
    public void restoreLink() {
        pt.getLink(linkId).setBandwidth(bw);
    }

}
