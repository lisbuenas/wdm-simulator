package wdmsim;

/**
 * This class represent a DC node with their physical resources
 * @author Felipe Lisboa
 */
public class DC {
    private int id;
    private int storageUnits;
    private int virtualMachines;
    private int freeStorageUnits;
    private int freeVirtualMachines;
    
    public DC(int id, int storageUnits, int virtualMachines){
        this.id = id;
        this.storageUnits = storageUnits;
        this.virtualMachines = virtualMachines;
        freeStorageUnits = storageUnits;
        freeVirtualMachines = virtualMachines;
    }
    
    /**
     * 
     * @return 
     */
    public int getID(){
        return id;
    }
    
    /**
     * 
     * @param machines
     * @param storage
     * @return 
     */
    public boolean hasAvailableResource(int machines, int storage){
        if(freeStorageUnits >= storage && freeVirtualMachines >= machines)
            return true;
        else
            return false;
    }
    
    /**
     * 
     * @param machines
     * @param storage
     * @return 
     */
    public void reserveResource(int machines, int storage){
        freeStorageUnits -= storage;
        freeVirtualMachines -= machines;
    }
    
    /**
     * 
     * @param machines
     * @param storage 
     */
    public void releaseResource(int machines, int storage){
        freeStorageUnits += storage;
        freeVirtualMachines += machines;
    }
}