# README #

O projeto é baseado no simulador WDMSim, disponível pela Unicamp http://www.lrc.ic.unicamp.br/wdmsim/about.html

## Funcionalidades acrescentadas no simulador ##

- Definição dos datacenters
- Ajuste no gerenciador de Eventos, contendo funcionalidade de reinserção
- Gerador de tráfego com as definições de nós destinos levando em consideração os Datacenters
- Inserção de falhas de acordo com a distribuição uniforme
- Inserção de um evento de falha e restauração de link, para aproveitamento da arquitetura do simulador



