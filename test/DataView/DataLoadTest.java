package DataView;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author felis
 */
public class DataLoadTest {
    
    public DataLoadTest() {
        
    }
    
    @Before
    public void setUp() {
        
    }

    /**
     * Test of getData method, of class DataLoad.
     */
    @Test
    public void testReadData() {
        System.out.println("getData");
        String csvFile = "teste.csv";
        DataLoad instance = new DataLoad();
        
        assertEquals("load;result", instance.readData("Teste"));
        
        assertNotEquals(instance.readData("Teste2"),"load;result1");
    }
    
    /**
     * Test of saveData method, of class DataLoad.
     */
    @Test
    public void testSaveData() {
        //save in different collumns
        
        System.out.println("saveData");
        DataLoad instance = new DataLoad();
        
        assertTrue(instance.saveData("testeWrite.csv",null,2));
        assertTrue(instance.saveData("testeWrite2.csv",null,2));
    }
}