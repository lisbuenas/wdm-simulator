package DataView;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Felipe Lisboa
 */
public class DataAnalisysTest {
    
    public DataAnalisysTest() {
        
    }
    
    @Before
    public void setUp() {
        
    }

    /**
     * Test of average method, of class DataAnalisys.
     */
    @Test
    public void testAverage() {
        System.out.println("average function");
        DataAnalisys da = new DataAnalisys();
        
        float testData1[][] = {{2,2},{2,2},{2,2}};
        float result1[] = {2,2};
        assertArrayEquals(result1, da.average(testData1), 0);
        
        float testData2[][] = {{2,2},{2,2},{2,2},{2,2}};
        float result2[] = {2,2};
        assertArrayEquals(result2, da.average(testData2), 0);
        
        float testData3[][] = {{2,2},{2,2},{2,2},{4,4}};
        float result3[] = {(float)2.5,(float)2.5};
        assertArrayEquals(result3, da.average(testData3), 0);
    }
    
    /**
     * Test of standardDeviation method, of class DataAnalisys.
     */
    @Test
    public void testStandardDeviation() {
        System.out.println("standard Deviation");
        // TODO review the generated test code and remove the default call to fail.
        DataAnalisys da = new DataAnalisys();
        
        float testData1[][] = {{2,2},{2,2}};
        float result1 = 0;
        assertEquals(result1, da.standardDeviation(testData1), 0);
        
        float testData2[][] = {{2,2},{2,2}};
        float result2 = 0;
        assertEquals(result2, da.standardDeviation(testData2), 0);
    }
    
    @Test
    public void testSetDataSet(){
        System.out.println("Test DataSet insertions");
        DataAnalisys da = new DataAnalisys();
        
        float dataset[][] = new float[2][2];
        
        da.setDataSet(dataset); //initialize
        
        da.addData(2, 1, 1);
        assertEquals(2.0, da.getData(1, 1),0);
        
        da.addData(3, 1, 1);
        assertNotEquals(2.0, da.getData(1, 1),0);
        
        float dataset2[][] = new float[2][3];
        da.setDataSet(dataset2);
        da.addData(5, 1, 2);// data, pos, iteration
        assertEquals(5.0, da.getData(1, 2),0);
    }
}