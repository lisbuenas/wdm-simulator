package wdmsim.rwa;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import wdmsim.Flow;
import wdmsim.PhysicalTopology;
import wdmsim.VirtualTopology;

/**
 *
 * @author Felipe Lisboa
 */
public class SLRCRWATest {
    
    public SLRCRWATest() {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of simulationInterface method, of class SLRCRWA.
     */
    @Test
    public void testSimulationInterface() {
        System.out.println("simulationInterface");
        PhysicalTopology pt = null;
        VirtualTopology vt = null;
        ControlPlaneForRWA cp = null;
        SLRCRWA instance = new SLRCRWA();
        instance.simulationInterface(pt, vt, cp);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of flowArrival method, of class SLRCRWA.
     */
    @Test
    public void testFlowArrival() {
        System.out.println("flowArrival");
        Flow flowInfo = null;
        SLRCRWA instance = new SLRCRWA();
        instance.flowArrival(flowInfo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of flowDeparture method, of class SLRCRWA.
     */
    @Test
    public void testFlowDeparture() {
        System.out.println("flowDeparture");
        long id = 0L;
        SLRCRWA instance = new SLRCRWA();
        instance.flowDeparture(id);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLinkError method, of class SLRCRWA.
     */
    @Test
    public void testSetLinkError() {
        System.out.println("setLinkError");
        int link = 0;
        SLRCRWA instance = new SLRCRWA();
        instance.setLinkError(link);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
